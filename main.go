package main

import (
	"context"
	"database/sql"
	"time"

	"mini-casino-builder/packages/RegisterInitModule"
	"github.com/heroiclabs/nakama-common/runtime"
)

func InitModule(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, initializer runtime.Initializer) error {
	initStart := time.Now()

	logger.Info("===================Mini Casino Builder 3.15.0=====================");

	RegisterInitModule.InitializeModules(ctx,logger, db,nk,initializer);

	logger.Info("Module loaded in %dms", time.Since(initStart).Milliseconds())
	logger.Info("Hello World!")

	return nil
}
