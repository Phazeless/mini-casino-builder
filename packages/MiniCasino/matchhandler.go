package MiniCasino
import (
	"context"
	"database/sql"
	"encoding/json"
	"mini-casino-builder/packages/Notification"
	"github.com/heroiclabs/nakama-common/runtime"
	"time"
)
type Player struct {
	username       string
	id             string
	Position  		PlayerPosition
	connected      bool
	disConnected   bool
	rejoinEndTime  int64
	isPlaying	   bool
}
type PlayerPosition struct{
	VelocityX    string
	VelocityY    string
	PositionX    string
	PositionY    string
}
type MatchState struct {
	presences   map[string]runtime.Presence
	playerCount int
	stopMatch bool
	Players   map[string]*Player
}
type Match struct{}
var PlayerID MatchState
type MatchRecievedData struct {
	VelocityX    string `json:"Velocity.x"`
	VelocityY    string `json:"Velocity.y"`
	PositionX    string `json:"position.x"`
	PositionY    string `json:"position.y"`
}
func (m *Match) MatchInit(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, params map[string]interface{}) (interface{}, int, string) {
	state := &MatchState{
		presences:   make(map[string]runtime.Presence),
		playerCount: 0,
		Players:  make(map[string]*Player),
	}
	tickRate := 60
	label := map[string]interface{}{
		"channel": "channel",
		"isOpen":  "true",
	}
	jsonLabel, _ := json.Marshal(label)
	return state, tickRate, string(jsonLabel)
}
func (m *Match) MatchJoinAttempt(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, presence runtime.Presence, metadata map[string]string) (interface{}, bool, string) {
	logger.Info("====Match Join Attempt=============")
	mState, _ := state.(*MatchState)
	accept := true
	errorMessage := ""
	if _, ok := mState.presences[presence.GetUserId()]; ok {
		accept = false
		errorMessage = "User already exists in another device"
	}

	newPlayer := true
	for _, player := range mState.Players {
		if player.username == presence.GetUsername(){
			newPlayer = false;
			errorMessage = ""
			accept = true
		}
	}

	if mState.playerCount >=100 && newPlayer{
		accept = false
		errorMessage = "This Channel is full"
	}
	return mState, accept, errorMessage
}
func (m *Match) MatchJoin(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, presences []runtime.Presence) interface{} {
	logger.Info("====Match Join=============")
	mState, _ := state.(*MatchState)
	userId := ""
	username := ""
	for _, p := range presences {
		mState.presences[p.GetUserId()] = p
		userId = p.GetUserId()
		username = p.GetUsername()
	}
	newPlayer := true
	for _, player := range mState.Players {
		if player.username == username{
			newPlayer = false;
		}
	}
	if(newPlayer){
		mState.playerCount++;
		Position := &PlayerPosition{}
		AddPlayer := &Player{username, userId,*Position,true,false,0,false}
		mState.Players[username] = AddPlayer
	}


	PlayerID.presences = mState.presences

	if mState.playerCount >= 100{
		label := map[string]interface{}{
			"channel": "channel",
			"isOpen":  "false",
		}
		jsonLabel, _ := json.Marshal(label)
		dispatcher.MatchLabelUpdate(string(jsonLabel))
	}
	return mState
}
func (m *Match) MatchLeave(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, presences []runtime.Presence) interface{} {
	mState, _ := state.(*MatchState)
	//username := ""
	for _, p := range presences {
		delete(mState.presences, p.GetUserId())
		//username = p.GetUsername()
		for _, player := range mState.Players {
			if player.username == p.GetUsername() && mState.Players[p.GetUsername()].connected {
				mState.Players[p.GetUsername()].connected = false
				mState.Players[p.GetUsername()].disConnected = true
				mState.Players[p.GetUsername()].rejoinEndTime = time.Now().Add(time.Second * time.Duration(30)).Unix()
				break
			}
		}
	}
	// for i,_ := range mState.Players{
	// 	if (i == username){
	// 		delete(mState.Players,username)
	// 	}
	// }
	// PlayerID.presences = mState.presences
	// if mState.playerCount <= 0 {
	// 	mState.stopMatch = true
	// }
	// if mState.playerCount < 100 {
	// 	label := map[string]interface{}{
	// 		"channel": "channel",
	// 		"isOpen":  "true",
	// 	}
	// 	jsonLabel, _ := json.Marshal(label)
	// 	dispatcher.MatchLabelUpdate(string(jsonLabel))
	// }
	return mState
}
func (m *Match) MatchLoop(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, messages []runtime.MatchData) interface{} {
	//logger.Info("%v", state)
	mState, _ := state.(*MatchState)

	if tick >= 14000 && len(mState.Players) <= 0 {
		mState.stopMatch = true
	}


	LeaveDisconnectedPlayers(mState, logger, dispatcher, nk, ctx, db)
	for _, message := range messages {
		decoded := &MatchRecievedData{}
		if err := json.Unmarshal(message.GetData(), &decoded); err != nil {
		}
		logger.Info("Received %v from %v", string(message.GetData()), message.GetUserId())
		reliable := true
		opCOde := message.GetOpCode()
		//logger.Printf("Total Presence are == %v ", len(state.Presences))
		switch opCOde {
		case 1:
			dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)
			updatePlayerPosition(decoded,message.GetUsername(),message.GetUserId(),[]runtime.Presence{message},mState,logger,dispatcher,nk,ctx);
			mState.Players[message.GetUsername()].connected = true
			mState.Players[message.GetUsername()].disConnected = false
		case 3: //Floor Broadcast
			dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)
		case 4: //Skin Broadcast
		dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)
		case 11:
			onLeave(message.GetUsername(),message.GetUserId(), mState, ctx, logger, dispatcher, nk, db)
		case 12:
			dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)
		case 13: // Emojie Broadcast
			dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)
		case 14:
			dispatcher.BroadcastMessage(message.GetOpCode(), message.GetData(), nil, message, reliable)

		default:
		}
	}
	if mState.stopMatch {
		return nil
	}
	return state
}
func (m *Match) MatchTerminate(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, graceSeconds int) interface{} {
	return state
}
func (m *Match) MatchSignal(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, dispatcher runtime.MatchDispatcher, tick int64, state interface{}, data string) (interface{}, string) {
	return state, data
}
type abc struct {
	Username string `json:"username"`
}
func updatePlayerPosition(data *MatchRecievedData,username string ,userId string ,presences []runtime.Presence, mState *MatchState, logger runtime.Logger, dispatcher runtime.MatchDispatcher, nk runtime.NakamaModule, ctx context.Context) {
		for _, player := range mState.Players {
			if(player.username == username){
				mState.Players[username].Position.PositionX =  data.PositionX;
				mState.Players[username].Position.PositionY =  data.PositionY;
				mState.Players[username].Position.VelocityX =  data.VelocityX;
				mState.Players[username].Position.VelocityY =  data.VelocityY;
			}
		}
}
func LeaveDisconnectedPlayers(state *MatchState, logger runtime.Logger, dispatcher runtime.MatchDispatcher, nk runtime.NakamaModule, ctx context.Context, db *sql.DB) {
	for _, player := range state.Players {
			if !player.connected && player.disConnected {
				if time.Now().Unix() >= player.rejoinEndTime {
					logger.Info("==============Disconnected Player Found===============");
					onLeave(player.username,player.id, state, ctx, logger, dispatcher, nk, db)
				}
			}
	}
}
func onLeave(username string,userId string, mState *MatchState, ctx context.Context, logger runtime.Logger, dispatcher runtime.MatchDispatcher, nk runtime.NakamaModule, db *sql.DB) {

	pos := &abc{username}
	out,_:= json.Marshal(pos)
	dispatcher.BroadcastMessage(51, out, nil,nil, true)
	mState.playerCount--;

	for i,_ := range mState.Players{
		if (i == username){
			delete(mState.Players,username)
		}
	}

	if mState.playerCount <= 0 {
		mState.stopMatch = true
	}

	if mState.playerCount <=99 {
		label := map[string]interface{}{
			"channel": "channel",
			"isOpen":  "true",
		}
		jsonLabel, _ := json.Marshal(label)
		dispatcher.MatchLabelUpdate(string(jsonLabel))
	}else{
		label := map[string]interface{}{
			"channel": "channel",
			"isOpen":  "false",
		}
		jsonLabel, _ := json.Marshal(label)
		dispatcher.MatchLabelUpdate(string(jsonLabel))

	}
}
func TestSendAll(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var input Notification.NotificationContent
	if err := json.Unmarshal([]byte(payload), &input); err != nil {
		return "", err
	}
	notifications := []*runtime.NotificationSend{}
	for _, p := range PlayerID.presences {
		notifications = append(notifications, &runtime.NotificationSend{
			UserID:     p.GetUserId(),
			Subject:    input.Subject,
			Content:    input.Content,
			Code:       input.Code,
			Sender:     input.Sender,
			Persistent: true,
		})
		err := nk.NotificationsSend(ctx, notifications)
		if err != nil {
			logger.Error("failed to send winner notifications: %v", err)
			return "", err
		}
	}
	out, err := json.Marshal(input)
	if err != nil {
		return "", err
	}
	return string(out), nil
}
