package data

import (
	"context"
	"database/sql"
	"encoding/json"
	"time"

	"github.com/heroiclabs/nakama-common/runtime"
)

type Detail struct {
	Type  string  `json:"type"`
	Value float64 `json:"value"`
}

type Shop struct {
	Name          string   `json:"name"`
	Product       []Detail `json:"product"`
	Price         Detail   `json:"price"`
	DiscountPrice Detail   `json:"discount_price"`
}

type ShopData struct {
	PackageShop []Shop `json:"package_shop"`
	DiamondShop []Shop `json:"diamond_shop"`
	CoinShop    []Shop `json:"coin_shop"`
}

type Schedule struct {
	StartDay int `json:"start_day"`
	EndDay   int `json:"end_day"`
}

type DiscountSchedule struct {
	CoinDiscountSchedule    []Schedule `json:"coin_schedule"`
	DiamondDiscountSchedule []Schedule `json:"diamond_schedule"`
	PackageDiscountSchedule []Schedule `json:"package_schedule"`
}

const (
	Monday = iota + 1
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

func GetDay(t time.Weekday) int {
	switch t {
	case time.Sunday:
		return int(Sunday)
	case time.Monday:
		return int(Monday)
	case time.Tuesday:
		return int(Tuesday)
	case time.Wednesday:
		return int(Wednesday)
	case time.Thursday:
		return int(Thursday)
	case time.Friday:
		return int(Friday)
	case time.Saturday:
		return int(Saturday)
	}

	return -1
}

func CreateShopReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var shops ShopData

	shops.CoinShop = []Shop{
		{
			Name: "250K coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 250000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 10,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 10,
			},
		},
		{
			Name: "1M coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 1000000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 30,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 30,
			},
		},
		{
			Name: "5M coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 5000000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 100,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 100,
			},
		},
		{
			Name: "10M coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 10000000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 150,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 150,
			},
		},
		{
			Name: "70M coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 70000000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 600,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 600,
			},
		},
		{
			Name: "400M coins",
			Product: []Detail{
				{
					Type:  "Coin",
					Value: 400000000,
				},
			},
			Price: Detail{
				Type:  "Diamond",
				Value: 2500,
			},
			DiscountPrice: Detail{
				Type:  "Diamond",
				Value: 2500,
			},
		},
	}

	shops.DiamondShop = []Shop{
		{
			Name: "Pile of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 500,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 4.99,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 4.99,
			},
		},
		{
			Name: "Heap of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 1200,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 9.99,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 9.99,
			},
		},
		{
			Name: "Box of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 3000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 24.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 19.9,
			},
		},
		{
			Name: "Bucket of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 6750,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 49.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 37.5,
			},
		},
		{
			Name: "Barrel of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 15000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 99.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 69.9,
			},
		},
		{
			Name: "Chest of Diamonds",
			Product: []Detail{
				{
					Type:  "Diamond",
					Value: 25000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 149.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 97.5,
			},
		},
	}

	shops.PackageShop = []Shop{
		{
			Name: "Growth Pack (1M Coins + 2500 Diamonds)",
			Product: []Detail{
				{
					Type:  "Coins",
					Value: 1000000,
				},
				{
					Type:  "Diamond",
					Value: 2500,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 19.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 19.9,
			},
		},
		{
			Name: "Wealth Pack (10M Coins + 5000 Diamonds)",
			Product: []Detail{
				{
					Type:  "Coins",
					Value: 10000000,
				},
				{
					Type:  "Diamond",
					Value: 5000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 39.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 29.9,
			},
		},
		{
			Name: "Deluxe Pack (100M Coins + 12000 Diamonds)",
			Product: []Detail{
				{
					Type:  "Coins",
					Value: 100000000,
				},
				{
					Type:  "Diamond",
					Value: 12000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 89.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 62.9,
			},
		},
		{
			Name: "Premium Pack (400M Coins + 20000 Diamonds)",
			Product: []Detail{
				{
					Type:  "Coins",
					Value: 400000000,
				},
				{
					Type:  "Diamond",
					Value: 20000,
				},
			},
			Price: Detail{
				Type:  "Money",
				Value: 119.9,
			},
			DiscountPrice: Detail{
				Type:  "Money",
				Value: 77.9,
			},
		},
	}

	s, err := json.Marshal(shops)

	if err != nil {
		return "", err
	}

	shopWrite := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Shop",
		Value:           string(s),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	var schedule DiscountSchedule

	schedule.DiamondDiscountSchedule = []Schedule{
		{
			StartDay: 6,
			EndDay:   7,
		},
	}

	schedule.PackageDiscountSchedule = []Schedule{
		{
			StartDay: 3,
			EndDay:   3,
		},
		{
			StartDay: 7,
			EndDay:   7,
		},
	}

	d, err := json.Marshal(schedule)

	if err != nil {
		return "", err
	}

	scheduleWrite := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Discount_Schedule",
		Value:           string(d),
		PermissionRead:  2,
		PermissionWrite: 0,
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{scheduleWrite, shopWrite})

	if err != nil {
		return "", err
	}

	return "", nil
}

func CheckDiscount(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var resp struct {
		CoinShop    bool             `json:"coin"`
		DiamondShop bool             `json:"diamond"`
		PackageShop bool             `json:"package"`
		Schedule    DiscountSchedule `json:"schedule"`
	}

	resp.CoinShop = false
	resp.DiamondShop = false
	resp.PackageShop = false

	objects, err := nk.StorageRead(ctx, []*runtime.StorageRead{{
		Collection: "System",
		Key:        "Discount_Schedule",
	}})

	if err != nil {
		logger.Error("StorageRead error: %v", err)
		return "", err
	}

	for _, object := range objects {
		switch object.GetKey() {
		case "Discount_Schedule":
			if err := json.Unmarshal([]byte(object.GetValue()), &resp.Schedule); err != nil {
				logger.Error("Unmarshal error: %v", err)
			}
		}
	}

	resp.CoinShop = CheckSchedule(resp.Schedule.CoinDiscountSchedule)
	resp.DiamondShop = CheckSchedule(resp.Schedule.DiamondDiscountSchedule)
	resp.PackageShop = CheckSchedule(resp.Schedule.PackageDiscountSchedule)

	response, err := json.Marshal(resp)

	if err != nil {
		return "", err
	}

	return string(response), nil
}

func CheckSchedule(s []Schedule) bool {
	loc, _ := time.LoadLocation("Asia/Bangkok")
	t := time.Now().In(loc).Weekday()

	if len(s) == 0 {
		return false
	}

	for _, schedule := range s {
		if GetDay(t) >= schedule.StartDay && GetDay(t) <= schedule.EndDay {
			return true
		}
	}

	return false
}