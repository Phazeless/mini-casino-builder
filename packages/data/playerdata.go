package data

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/heroiclabs/nakama-common/runtime"
)

type DailyRewardDetail struct {
	Type  string `json:"type"`
	Value uint64 `json:"value"`
}

type DailyRewardData struct {
	Day    int                 `json:"day"`
	Data   []DailyRewardDetail `json:"data"`
	Status bool                `json:"hasClaimed"`
}

type DailyReward struct {
	LastClaimUnix   int64             `json:"last_claim_unix"`
	CurrentClaimDay int               `json:"current_day"`
	DailyRewards    []DailyRewardData `json:"daily_rewards"`
}

type MaintenanceAndPatch struct {
	PatchVersion string `json:"Version"`
	Maintenance  bool   `json:"IsMaintenance"`
}

var maintenanceAndPatch MaintenanceAndPatch

func CreateMaintenanceAndPatch(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var p MaintenanceAndPatch
	if err := json.Unmarshal([]byte(payload), &p); err != nil {
		return "", err
	}

	maintenanceAndPatch.Maintenance = p.Maintenance
	maintenanceAndPatch.PatchVersion = p.PatchVersion
	out, err := json.Marshal(maintenanceAndPatch)

	if err != nil {
		return "", err
	}
	data := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "MaintenanceAndPatch",
		Value:           string(out),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{data})

	if err != nil {
		return "", err
	}
	return string(out), nil
}

var dailyReward DailyReward

func CreateDailyReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	logger.Debug("====CreateDailyReward=============")

	dailyReward.CurrentClaimDay = 1
	dailyReward.DailyRewards = []DailyRewardData{
		{
			Day: 1,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 50000,
				},
			},
			Status: false,
		},
		{
			Day: 2,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 100000,
				},
			},
			Status: false,
		},
		{
			Day: 3,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 300000,
				},
				{
					Type:  "Diamond",
					Value: 100,
				},
			},
			Status: false,
		},
		{
			Day: 4,
			Data: []DailyRewardDetail{
				{
					Type:  "Diamond",
					Value: 150,
				},
			},
			Status: false,
		},
		{
			Day: 5,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 1000000,
				},
				{
					Type:  "Diamond",
					Value: 250,
				},
			},
			Status: false,
		},
		{
			Day: 6,
			Data: []DailyRewardDetail{
				{
					Type:  "Diamond",
					Value: 300,
				},
			},
			Status: false,
		},
		{
			Day: 7,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 10000000,
				},
				{
					Type:  "Diamond",
					Value: 500,
				},
			},
			Status: false,
		},
		{
			Day: 8,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 50000,
				},
			},
			Status: false,
		},
		{
			Day: 9,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 100000,
				},
			},
			Status: false,
		},
		{
			Day: 10,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 300000,
				},
				{
					Type:  "Diamond",
					Value: 100,
				},
			},
			Status: false,
		},
		{
			Day: 11,
			Data: []DailyRewardDetail{
				{
					Type:  "Diamond",
					Value: 150,
				},
			},
			Status: false,
		},
		{
			Day: 12,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 1000000,
				},
				{
					Type:  "Diamond",
					Value: 250,
				},
			},
			Status: false,
		},
		{
			Day: 13,
			Data: []DailyRewardDetail{
				{
					Type:  "Diamond",
					Value: 300,
				},
			},
			Status: false,
		},
		{
			Day: 14,
			Data: []DailyRewardDetail{
				{
					Type:  "Coin",
					Value: 10000000,
				},
				{
					Type:  "Diamond",
					Value: 500,
				},
			},
			Status: false,
		},
	}

	out, err := json.Marshal(dailyReward)

	if err != nil {
		return "", err
	}

	data := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Daily",
		Value:           string(out),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{data})

	if err != nil {
		return "", err
	}

	return string(out), nil
}

func ResetDailyReward(ctx context.Context, logger runtime.Logger, nk runtime.NakamaModule) (DailyReward, error) {
	var resp DailyReward
	data := []*runtime.StorageRead{
		{
			Collection: "System",
			Key:        "Daily",
		},
	}

	t, err := nk.StorageRead(ctx, data)

	if err != nil {
		logger.Error("UnMarshal error : %v", err)
		return DailyReward{}, err
	}

	for _, object := range t {
		switch object.GetKey() {
		case "Daily":
			if err := json.Unmarshal([]byte(object.GetValue()), &resp); err != nil {
				logger.Error("UnMarshal error : %v", err)
				return DailyReward{}, err
			}
		}
	}

	return resp, nil
}

type PlayerPropertiesData struct {
	Gender        int     `json:"m_Gender"`
	Floor         int     `json:"m_Floor"`
	VIP           int     `json:"m_VIP"`
	FloorUnlocked []bool  `json:"m_FloorUnlocked"`
	Coin          int64   `json:"m_Coin"`
	ChihuahuaCoin float64 `json:"m_ChihuahuaCoin"`
	AsmazingStock float64 `json:"m_AsmazingStock"`
	MirocokStock  float64 `json:"m_MirocokStock"`
	BiteCoin      float64 `json:"m_BiteCoin"`
	TelsaStock    float64 `json:"m_TelsaStock"`
}

type PlayerProperty struct {
	PlayerProperties PlayerPropertiesData `json:"player_properties"`
}

var playerProperties PlayerProperty

func GetPlayerProperties() PlayerProperty {
	playerProperties.PlayerProperties = PlayerPropertiesData{
		Gender: 0,
		Floor:  0,
		VIP:    0,
		FloorUnlocked: []bool{
			true,
			true,
			false,
			false,
			false,
			false,
		},
		Coin:          200000,
		ChihuahuaCoin: 0,
		AsmazingStock: 0,
		MirocokStock:  0,
		BiteCoin:      0,
		TelsaStock:    0,
	}

	return playerProperties
}

type MissionData struct {
	Earnings          int64       `json:"m_Earnings"`
	TotalCoin         int64       `json:"m_TotalCoin"`
	BetWinnings       [][]float64 `json:"m_BetWinnings"`
	JackpotWinnings   []int       `json:"m_JackpotWinnings"`
	TotalGameWinnings [][]int     `json:"m_TotalGameWinnings"`
}

type Mission struct {
	MissionData MissionData `json:"mission_data"`
}

var missionData Mission

func GetMissionData() Mission {
	missionData.MissionData = MissionData{
		Earnings:  0,
		TotalCoin: 0,
		BetWinnings: [][]float64{
			{
				0,
				0,
				0,
				0,
				0,
			},
		},
		JackpotWinnings: []int{
			0,
			0,
			0,
			0,
			0,
		},
		TotalGameWinnings: [][]int{
			{
				0,
				0,
				0,
				0,
				0,
			},
		},
	}

	return missionData
}
