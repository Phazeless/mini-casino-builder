package data

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/heroiclabs/nakama-common/runtime"
)

type TournamentRewardDetail struct {
	RewardType string `json:"reward_type"`
	Value      int64  `json:"value"`
}

type RankRange struct {
	Min         int                      `json:"min_rank"`
	Max         int                      `json:"max_rank"`
	ListRewards []TournamentRewardDetail `json:"list_rewards"`
}

// 1 monthly
// 2 weekly
type TournamentRewardData struct {
	Type int         `json:"tournament_type"`
	Rank []RankRange `json:"detail"`
}

type TournamentRewards struct {
	Data []TournamentRewardData `json:"data"`
}

func CreateTournamentReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var tournament TournamentRewards

	tournament.Data = []TournamentRewardData{
		{
			Type: 1,
			Rank: []RankRange{
				{
					Min: 1,
					Max: 1,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      200000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      2000,
						},
					},
				},
				{
					Min: 2,
					Max: 2,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      100000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      1000,
						},
					},
				},
				{
					Min: 3,
					Max: 3,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      50000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      500,
						},
					},
				},
				{
					Min: 4,
					Max: 10,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      10000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      500,
						},
					},
				},
				{
					Min: 11,
					Max: 20,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      5000000,
						},
					},
				},
				{
					Min: 21,
					Max: 100,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      2500000,
						},
					},
				},
			},
		},
		{
			Type: 2,
			Rank: []RankRange{
				{
					Min: 1,
					Max: 1,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      50000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      500,
						},
					},
				},
				{
					Min: 2,
					Max: 2,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      25000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      250,
						},
					},
				},
				{
					Min: 3,
					Max: 3,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      10000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      100,
						},
					},
				},
				{
					Min: 4,
					Max: 10,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      5000000,
						},
						{
							RewardType: "DIAMOND",
							Value:      50,
						},
					},
				},
				{
					Min: 11,
					Max: 20,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      3000000,
						},
					},
				},
				{
					Min: 21,
					Max: 100,
					ListRewards: []TournamentRewardDetail{
						{
							RewardType: "COIN",
							Value:      1000000,
						},
					},
				},
			},
		},
	}

	t, err := json.Marshal(tournament)

	if err != nil {
		return "", err
	}

	tournamentWrite := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Tournament_Reward",
		Value:           string(t),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{tournamentWrite})
	if err != nil {
		return "", err
	}

	return "", nil
}
