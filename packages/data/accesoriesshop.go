package data

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/heroiclabs/nakama-common/runtime"
)

type Accesories struct {
	Name      string `json:"name"`
	PriceType string `json:"price_type"`
	Price     int64  `json:"price"`
}

type AccesoriesShop struct {
	Wrist    []Accesories `json:"wrist_shop"`
	Necklace []Accesories `json:"necklace_shop"`
	Hat      []Accesories `json:"hat_shop"`
	Glasses  []Accesories `json:"glasses_shop"`
	Pin      []Accesories `json:"pin_shop"`
	Clothes  []Accesories `json:"clothes"`
}

func CreateAccesoriesPriceList(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var shops AccesoriesShop
	logger.Debug("====CreateAccesoriesPriceList=============");

	shops.Wrist = []Accesories{
		{
			Name:      "Accessories/Wrist_1",
			PriceType: "DIAMOND",
			Price:     800,
		},
		{
			Name:      "Accessories/Wrist_2",
			PriceType: "COIN",
			Price:     500000,
		},
		{
			Name:      "Accessories/Wrist_3",
			PriceType: "DIAMOND",
			Price:     600,
		},
		{
			Name:      "Accessories/Wrist_4",
			PriceType: "DIAMOND",
			Price:     1200,
		},
		{
			Name:      "Accessories/Wrist_5",
			PriceType: "DIAMOND",
			Price:     800,
		},
		{
			Name:      "Accessories/Wrist_6",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Wrist_7",
			PriceType: "DIAMOND",
			Price:     500,
		},
		{
			Name:      "Accessories/Wrist_8",
			PriceType: "COIN",
			Price:     1000000,
		},
	}

	shops.Necklace = []Accesories{
		{
			Name:      "Accessories/Necklace_1",
			PriceType: "COIN",
			Price:     10000000,
		},
		{
			Name:      "Accessories/Necklace_2",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Necklace_3",
			PriceType: "DIAMOND",
			Price:     3000,
		},
		{
			Name:      "Accessories/Necklace_4",
			PriceType: "DIAMOND",
			Price:     800,
		},
		{
			Name:      "Accessories/Necklace_5",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Necklace_6",
			PriceType: "DIAMOND",
			Price:     600,
		},
		{
			Name:      "Accessories/Necklace_7",
			PriceType: "DIAMOND",
			Price:     300,
		},
		{
			Name:      "Accessories/Necklace_8",
			PriceType: "COIN",
			Price:     500000,
		},
	}

	shops.Hat = []Accesories{
		{
			Name:      "Accessories/Hat_1",
			PriceType: "COIN",
			Price:     10000000,
		},
		{
			Name:      "Accessories/Hat_2",
			PriceType: "DIAMOND",
			Price:     500,
		},
		{
			Name:      "Accessories/Hat_3",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Hat_4",
			PriceType: "DIAMOND",
			Price:     2500,
		},
		{
			Name:      "Accessories/Hat_5",
			PriceType: "COIN",
			Price:     1000000,
		},
		{
			Name:      "Accessories/Hat_6",
			PriceType: "DIAMOND",
			Price:     650,
		},
		{
			Name:      "Accessories/Hat_7",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Hat_8",
			PriceType: "DIAMOND",
			Price:     1800,
		},
	}

	shops.Glasses = []Accesories{
		{
			Name:      "Accessories/Glasses_1",
			PriceType: "COIN",
			Price:     3000000,
		},
		{
			Name:      "Accessories/Glasses_2",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Glasses_3",
			PriceType: "COIN",
			Price:     800000,
		},
		{
			Name:      "Accessories/Glasses_4",
			PriceType: "DIAMOND",
			Price:     750,
		},
		{
			Name:      "Accessories/Glasses_5",
			PriceType: "DIAMOND",
			Price:     1500,
		},
		{
			Name:      "Accessories/Glasses_6",
			PriceType: "DIAMOND",
			Price:     1000,
		},
		{
			Name:      "Accessories/Glasses_7",
			PriceType: "DIAMOND",
			Price:     1500,
		},
		{
			Name:      "Accessories/Glasses_8",
			PriceType: "DIAMOND",
			Price:     2000,
		},
	}

	shops.Pin = []Accesories{
		{
			Name:      "Accessories/Pin_1",
			PriceType: "COIN",
			Price:     500000,
		},
		{
			Name:      "Accessories/Pin_2",
			PriceType: "DIAMOND",
			Price:     500,
		},
		{
			Name:      "Accessories/Pin_3",
			PriceType: "DIAMOND",
			Price:     600,
		},
		{
			Name:      "Accessories/Pin_4",
			PriceType: "DIAMOND",
			Price:     600,
		},
		{
			Name:      "Accessories/Pin_5",
			PriceType: "DIAMOND",
			Price:     800,
		},
		{
			Name:      "Accessories/Pin_6",
			PriceType: "DIAMOND",
			Price:     1200,
		},
		{
			Name:      "Accessories/Pin_7",
			PriceType: "COIN",
			Price:     5000000,
		},
		{
			Name:      "Accessories/Pin_8",
			PriceType: "DIAMOND",
			Price:     350,
		},
	}

	shops.Clothes = []Accesories{
		{
			Name:      "Clothes_1",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_2",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_3",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_4",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_5",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_6",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_7",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_8",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_9",
			PriceType: "COIN",
			Price:     300,
		},
		{
			Name:      "Clothes_10",
			PriceType: "COIN",
			Price:     300,
		},
	}

	s, err := json.Marshal(shops)

	if err != nil {
		return "", err
	}

	shopWrite := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Accesories_Shop",
		Value:           string(s),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{shopWrite})

	if err != nil {
		return "", err
	}

	return "", nil
}
