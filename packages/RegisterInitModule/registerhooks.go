package RegisterInitModule

import (
	"context"
	"database/sql"
	"mini-casino-builder/packages/Tournament"
	"github.com/heroiclabs/nakama-common/runtime"
)

func RegisterHooks(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, initializer runtime.Initializer) error {

	if err := initializer.RegisterAfterAuthenticateFacebook(AfterAuthenticateFacebook); err != nil {
		logger.Error("Unable to register: %v", err)
		return err
	}

	if err := initializer.RegisterAfterAuthenticateGoogle(AfterAuthenticateGoogle); err != nil {
		logger.Error("Unable to register: %v", err)
		return err
	}

	if err := initializer.RegisterAfterAuthenticateDevice(AfterAuthenticateDevice); err != nil {
		logger.Error("Unable to register: %v", err)
		return err
	}

	if err := initializer.RegisterTournamentEnd(Tournament.ClaimReward); err != nil {
		logger.Error("Unable to Send Reward: %v", err)
		return err
	}

	return nil;
}