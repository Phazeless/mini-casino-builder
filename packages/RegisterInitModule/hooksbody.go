package RegisterInitModule

import (
	"context"
	"database/sql"

	"mini-casino-builder/packages/Authentication/InitializeUser"

	"github.com/heroiclabs/nakama-common/api"
	"github.com/heroiclabs/nakama-common/runtime"
)
func AfterAuthenticateFacebook(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, out *api.Session, in *api.AuthenticateFacebookRequest) error {
	if out.Created {
		InitializeUser.CreateUserStorageEngine(ctx, logger, nk)
	}
	return nil
}

func AfterAuthenticateGoogle(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, out *api.Session, in *api.AuthenticateGoogleRequest) error {
	if out.Created {
		InitializeUser.CreateUserStorageEngine(ctx, logger, nk)
	}

	return nil
}
func AfterAuthenticateDevice(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, out *api.Session, in *api.AuthenticateDeviceRequest) error {
	if out.Created {
		InitializeUser.CreateUserStorageEngine(ctx, logger, nk)
	}

	return nil
}