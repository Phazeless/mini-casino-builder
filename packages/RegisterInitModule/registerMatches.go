package RegisterInitModule

import (

	"github.com/heroiclabs/nakama-common/runtime"
	"database/sql"
	"context"

	"mini-casino-builder/packages/MiniCasino"
)



func RegisterMatches(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, initializer runtime.Initializer) error {

	logger.Info("=========================== RegisterMatches ==============================");

	// Register as match handler, this call should be in InitModule.
	if err := initializer.RegisterMatch("Channel", func(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule) (runtime.Match, error) {
		return &MiniCasino.Match{}, nil
	}); err != nil {
		logger.Error("Unable to register: %v", err)
		return err
	}

	return nil
}
