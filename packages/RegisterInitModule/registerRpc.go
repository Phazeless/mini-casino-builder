package RegisterInitModule

import (
	"context"
	"database/sql"

	"github.com/heroiclabs/nakama-common/runtime"

	"mini-casino-builder/packages/DailyReward"
	"mini-casino-builder/packages/MatchMaking/MatchList"
	"mini-casino-builder/packages/MiniCasino"
	"mini-casino-builder/packages/Notification"
	"mini-casino-builder/packages/Tournament"
	"mini-casino-builder/packages/data"
)

func RegisterRPC(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, initializer runtime.Initializer) error {

	logger.Info("=========================== RegisterRPC ==============================")

	if err := initializer.RegisterRpc("ClaimDailyReward", DailyReward.RpcClaimDailyReward); err != nil {
		logger.Error("Cannot Claim Daily Reward: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CheckDiscount", data.CheckDiscount); err != nil {
		logger.Error("Cannot Check Discount: %v", err)
		return err
	}

	// Register as matchmaker matched hook, this call should be in InitModule.
	// if err := initializer.RegisterMatchmakerMatched(MakeMatch); err != nil {
	// 	logger.Error("Unable to register: %v", err)
	// 	return err
	// }

	if err := initializer.RegisterRpc("ListMatch", MatchList.Get_MatchId); err != nil {
		logger.Error("Unable to get MatchId: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateTournament", Tournament.CreateTournament); err != nil {
		logger.Error("Unable to create tournament: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateLeaderboard", Tournament.CreateLeaderboard); err != nil {
		logger.Error("Unable to create leaderboard: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateTournamentReward", data.CreateTournamentReward); err != nil {
		logger.Error("Unable to create Tournament: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateShopReward", data.CreateShopReward); err != nil {
		logger.Error("Unable to create Tournament: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateAccesoriesPriceList", data.CreateAccesoriesPriceList); err != nil {
		logger.Error("Unable to create Accesories Price List: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("SendNotification", Notification.SendNotificationAPI); err != nil {
		logger.Error("Unable to create Tournament: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("ChangeDailyReward", DailyReward.RpcChangeDailyRewardContent); err != nil {
		logger.Error("Unable to change Daily Reward: %v", err)
		return err
	}

	if err := initializer.RegisterRpc("CreateDailyReward", data.CreateDailyReward); err != nil {
		logger.Error("Unable to create Daily Reward: %v", err)
		return err
	}
	if err := initializer.RegisterRpc("MaintenanceAndPatch", data.CreateMaintenanceAndPatch); err != nil {
		logger.Error("MaintenanceAndPatch: %v", err)
		return err
	}
	if err := initializer.RegisterRpc("TestSendAll", MiniCasino.TestSendAll); err != nil {
		logger.Error("TestSendAll: %v", err)
		return err
	}

	return nil

}
