package RegisterInitModule

import (
	"context"
	"database/sql"
	"github.com/heroiclabs/nakama-common/runtime"
)


func InitializeModules(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, initializer runtime.Initializer) error {
	RegisterMatches(ctx,logger, db,nk,initializer)
	RegisterRPC(ctx,logger, db,nk,initializer)
	RegisterHooks(ctx,logger, db,nk,initializer)
	return nil;
}