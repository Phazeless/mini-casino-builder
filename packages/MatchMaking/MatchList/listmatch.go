package MatchList

import (
	"context"
	"database/sql"
	"encoding/json"
	//"strconv"

	"github.com/heroiclabs/nakama-common/runtime"
)

type MatchData struct {
	MatchId     string `json:"match_id"`
	PlayerCount int    `json:"player_count"`
	Limit       int    `json:"player_limit"`
	Label       string  `json:"label"`
}

type MatchList struct {
	Matches []MatchData `json:"data"`
}

func Get_MatchId(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {

	logger.Info("===========================LIST MATCH=============================="+payload);
	matchList := MatchList{}

	limit := 100
	isAuthoritative := true
	label := "" //input["channel"]""
	min_size := 0
	max_size := 99
	query := "+label.isOpen:true"

	logger.Info("label : %s", label)
	matches, err := nk.MatchList(ctx, limit, isAuthoritative, label, &min_size, &max_size, query)
	if err != nil {
		logger.WithField("err", err).Error("Match list Error.")
		return "", err
	}
	isMatch:=false;
	for i,_ := range matches {
		logger.Info("",i);
		isMatch=true
	}
	modulename := "Channel"
	if (!isMatch) {
		params := map[string]interface{}{
		}

		if matchId, err := nk.MatchCreate(ctx, modulename, params); err == nil {
			label:=map[string]interface{}{
				"channel": "channel",
				"isOpen": "true",
			}
			out, _ := json.Marshal(label)
			matchList.Matches = append(matchList.Matches, MatchData{
				MatchId:     matchId,
				PlayerCount: 0,
				Limit:       100,
				Label:        string(out),
			})
		} else {
			logger.Info(err.Error())
			return "",err;
		}
	}
	matches,_  = nk.MatchList(ctx, limit, isAuthoritative, label, &min_size, &max_size,"*")
	for _, match := range matches {
			matchList.Matches = append(matchList.Matches, MatchData{
				MatchId:     match.MatchId,
				Limit:       100,
				PlayerCount: int(match.Size),
				Label:        match.GetLabel().GetValue(),
			})
	}

	if out, err := json.Marshal(matchList); err == nil {
		return string(out), nil
	} else {
		logger.Info(err.Error())
	}

	return "", nil
}
