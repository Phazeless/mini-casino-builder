package Notification

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/heroiclabs/nakama-common/runtime"
)

// 1 = with rewards
// 2 = just message
type NotificationContent struct {
	UserId  string                 `json:"user_id"`
	Subject string                 `json:"subject"`
	Content map[string]interface{} `json:"data"`
	Code    int                    `json:"code"`
	Sender  string                 `json:"sender"`
}
type NotifiAll struct {
	Subject string                 `json:"subject"`
	Content map[string]interface{} `json:"data"`
	Code    int                    `json:"code"`
}

// func SendNotification(ctx context.Context, logger runtime.Logger, nk runtime.NakamaModule, Content NotificationContent) (string, error) {
// 	notifications := []*runtime.NotificationSend{}

// 	notifications = append(notifications, &runtime.NotificationSend{
// 		UserID:     Content.UserId,
// 		Subject:    Content.Subject,
// 		Content:    Content.Content,
// 		Code:       Content.Code,
// 		Sender:     Content.Sender,
// 		Persistent: true,
// 	})

// 	err := nk.NotificationsSend(ctx, notifications)

// 	if err != nil {
// 		logger.Error("failed to send winner notifications: %v", err)
// 		return "", err
// 	}

// 	out, err := json.Marshal(Content)

// 	if err != nil {
// 		return "", err
// 	}

// 	return string(out), nil
// }

func SendNotificationAPI(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var input NotificationContent

	if err := json.Unmarshal([]byte(payload), &input); err != nil {
		return "", err
	}
	notifications := []*runtime.NotificationSend{}

	notifications = append(notifications, &runtime.NotificationSend{
		UserID:     input.UserId,
		Subject:    input.Subject,
		Content:    input.Content,
		Code:       input.Code,
		Sender:     input.Sender,
		Persistent: true,
	})

	err := nk.NotificationsSend(ctx, notifications)

	if err != nil {
		logger.Error("failed to send winner notifications: %v", err)
		return "", err
	}

	out, err := json.Marshal(input)

	if err != nil {
		return "", err
	}

	return string(out), nil
}
