package DailyReward

import (
	"context"
	"database/sql"
	"encoding/json"
	"mini-casino-builder/packages/data"
	"time"

	"github.com/heroiclabs/nakama-common/api"
	"github.com/heroiclabs/nakama-common/runtime"
)

func CanClaimDailyRewards(d data.DailyReward) bool {
	loc, _ := time.LoadLocation("Asia/Bangkok")
	t := time.Now().In(loc)
	midnight := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, loc)
	return time.Unix(d.LastClaimUnix, 0).Before(midnight)
}

func GetLastDailyRewards(ctx context.Context, logger runtime.Logger, nk runtime.NakamaModule, payload string) (data.DailyReward, *api.StorageObject, error) {
	var d data.DailyReward

	d.LastClaimUnix = 0

	userId, _ := ctx.Value(runtime.RUNTIME_CTX_USER_ID).(string)

	objects, err := nk.StorageRead(ctx, []*runtime.StorageRead{{
		Collection: "Reward",
		Key:        "Daily",
		UserID:     userId,
	}})

	if err != nil {
		logger.Error("StorageRead error: %v", err)
		return d, nil, err
	}

	var o *api.StorageObject
	for _, object := range objects {
		switch object.GetKey() {
		case "Daily":
			if err := json.Unmarshal([]byte(object.GetValue()), &d); err != nil {
				logger.Error("Unmarshal error: %v", err)
				return d, nil, err
			}
			return d, object, nil
		}
	}

	return d, o, nil
}

func RpcCanClaimDailyReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var Response struct {
		CanClaimDailyRewards bool `json:"CanClaimDailyReward"`
	}

	DailyReward, _, err := GetLastDailyRewards(ctx, logger, nk, payload)

	if err != nil {
		logger.Error("Marshal error %v", err)
		return "", err
	}

	Response.CanClaimDailyRewards = CanClaimDailyRewards(DailyReward)

	out, err := json.Marshal((Response))

	if err != nil {
		logger.Error("Marshal error : %v", err)
		return "", err
	}

	return string(out), nil
}

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

func RpcClaimDailyReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var resp struct {
		Data []data.DailyRewardDetail `json:"data"`
	}

	userId, _ := ctx.Value(runtime.RUNTIME_CTX_USER_ID).(string)

	dailyReward, _, err := GetLastDailyRewards(ctx, logger, nk, payload)

	if err != nil {
		logger.Error("Error getting daily rewards : %v", err)
		return "", err
	}

	if CanClaimDailyRewards(dailyReward) {
		resp.Data = dailyReward.DailyRewards[dailyReward.CurrentClaimDay-1].Data
		dailyReward.DailyRewards[dailyReward.CurrentClaimDay-1].Status = true
		loc, _ := time.LoadLocation("Asia/Bangkok")
		dailyReward.LastClaimUnix = time.Now().In(loc).Unix()
		dailyReward.CurrentClaimDay++

		if dailyReward.CurrentClaimDay >= 14 {
			dailyReward, err = data.ResetDailyReward(ctx, logger, nk)
			if err != nil {
				return "", err
			}
		}

		object, err := json.Marshal(dailyReward)

		if err != nil {
			logger.Error("Marshal error : %v", err)
			return "", err
		}

		_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{{
			Collection:      "Reward",
			Key:             "Daily",
			UserID:          userId,
			Value:           string(object),
			PermissionRead:  1,
			PermissionWrite: 1,
		}})

		if err != nil {
			logger.Error("StorageWrite Error: %v", err)
			return "", err
		}

		out, err := json.Marshal(resp)

		if err != nil {
			logger.Error("Marshal error : %v", err)
			return "", err
		}

		return string(out), nil
	}

	return "", &errorString{"Already Claimed"}
}

func RpcChangeDailyRewardContent(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var d data.DailyRewardData

	if err := json.Unmarshal([]byte(payload), &d); err != nil {
		return "", err
	}

	out, err := json.Marshal(d)

	if err != nil {
		return "", err
	}

	data := &runtime.StorageWrite{
		Collection:      "System",
		Key:             "Daily",
		Value:           string(out),
		PermissionRead:  2, // Only the server and owner can read
		PermissionWrite: 0, // Only the server can write
	}

	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{data})

	if err != nil {
		return "", err
	}

	return string(out), nil
}
