package Tournament

import (
	"context"
	"database/sql"
	"encoding/json"
	"mini-casino-builder/packages/data"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/heroiclabs/nakama-common/api"
	"github.com/heroiclabs/nakama-common/runtime"
	"github.com/mitchellh/mapstructure"
)

type TournamentDetails struct {
	Type          string `json:"type"`
	Category      int    `json:"category"`
	Duration      int    `json:"duration"`
	ResetSchedule string `json:"reset_schedule"`
}

func CreateTournament(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {
	var input TournamentDetails
	logger.Warn(payload)
	if err := json.Unmarshal([]byte(payload), &input); err != nil {
		logger.Warn("%v", input)
		return "", err
	}

	logger.Warn("%v", input)

	id := uuid.New()

	authorative := false
	sortOrder := "desc"                  // one of: "desc", "asc"
	operator := "incr"                   // one of: "best", "set", "incr"
	resetSchedule := input.ResetSchedule // noon UTC each day
	metadata := map[string]interface{}{}
	title := input.Type //"Monthly Competition"
	description := "Dash past your opponents for high scores and big rewards!"
	category := input.Category
	startTime := int(time.Now().UTC().Unix()) // start now
	endTime := 0                              // never end, repeat the tournament each day forever
	duration := input.Duration                // in seconds
	maxSize := 999999                         // first 10,000 players who join
	maxNumScore := 99999999                   // each player can have 3 attempts to score
	joinRequired := false                     // must join to compete
	err := nk.TournamentCreate(ctx, id.String(), authorative, sortOrder, operator, resetSchedule, metadata, title, description, category, startTime, endTime, duration, maxSize, maxNumScore, joinRequired)

	if err != nil {
		logger.Error("unable to create tournament: %q", err.Error())
		return "", runtime.NewError("failed to create tournament", 3)
	}

	return "", nil
}

func ClaimReward(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, tournament *api.Tournament, end int64, reset int64) error {
	var tournamentRewards data.TournamentRewards

	notifications := []*runtime.NotificationSend{}

	r := []*runtime.StorageRead{
		{
			Collection: "System",
			Key:        "Tournament_Reward",
		},
	}

	t, err := nk.StorageRead(ctx, r)

	if err != nil {
		return err
	}

	for _, object := range t {
		switch object.GetKey() {
		case "Tournament_Reward":
			if err := json.Unmarshal([]byte(object.GetValue()), &tournamentRewards); err != nil {
				logger.Error("Unmarshal error: %v", err)
				return err
			}
		}
	}

	records, _, _, _, err := nk.TournamentRecordsList(ctx, tournament.Id, []string{}, 100, "", reset)

	if err != nil {
		return err
	}

	for _, val := range tournamentRewards.Data {
		if val.Type == int(tournament.Category) {
			for _, data := range val.Rank {
				for i, _ := range records {
					if i >= (data.Min-1) && i < (data.Max) {
						content := map[string]interface{}{}
						err := mapstructure.Decode(data, &content)
						//err := err ;
						if err != nil {
							return err
						}
						content["message"] = "You Have Received Rewards for having rank " + strconv.Itoa(i) + " in " + tournament.Title + "."

						notifications = append(notifications, &runtime.NotificationSend{
							UserID:     records[i].OwnerId,
							Subject:    "Tournament Winner",
							Content:    content,
							Code:       1,
							Sender:     "",
							Persistent: true,
						})
					}
				}
			}

		}
	}

	nk.NotificationsSend(ctx, notifications)
	return nil
}

func CreateLeaderboard(ctx context.Context, logger runtime.Logger, db *sql.DB, nk runtime.NakamaModule, payload string) (string, error) {

	id := uuid.New()

	authorative := false
	sortOrder := "desc" // one of: "desc", "asc"
	operator := "incr"  // one of: "best", "set", "incr"
	resetSchedule := "" // noon UTC each day
	metadata := map[string]interface{}{}

	err := nk.LeaderboardCreate(ctx, id.String(), authorative, sortOrder, operator, resetSchedule, metadata)

	if err != nil {
		logger.Error("unable to create tournament: %q", err.Error())
		return "", runtime.NewError("failed to create tournament", 3)
	}
	return "", nil
}
