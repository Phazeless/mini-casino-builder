package InitializeUser
import (
	"context"
	"encoding/json"
	"mini-casino-builder/packages/data"
	"github.com/heroiclabs/nakama-common/runtime"
	"math/rand"
	"time"
)
func CreateUserStorageEngine(ctx context.Context, logger runtime.Logger, nk runtime.NakamaModule) error {
	logger.Debug("====create User Storage Engine=============");
	UpdateAvatar(ctx,logger,nk);
	userId, _ := ctx.Value(runtime.RUNTIME_CTX_USER_ID).(string)
	d, err := data.ResetDailyReward(ctx, logger, nk)
	if err != nil {
		logger.Error("Error get daily rewards %v", err)
		return err
	}
	dailyReward, err := json.Marshal(d)
	if err != nil {
		logger.Error("Error marshal create user %v", err)
		return err
	}
	dailyWrite := &runtime.StorageWrite{
		Collection:      "Reward",
		Key:             "Daily",
		UserID:          userId,
		Value:           string(dailyReward),
		PermissionRead:  1, // Only the server and owner can read
		PermissionWrite: 1, // Only the server can write
	}
	// playerDataWrite := &runtime.StorageWrite{
	// 	Collection:      "PlayerData",
	// 	Key:             "Player_Properties",
	// 	UserID:          userId,
	// 	Value:           string(playerProperties),
	// 	PermissionRead:  2, // Only the server and owner can read
	// 	PermissionWrite: 1, // Only the server can write
	// }
	// missionDataWrite := &runtime.StorageWrite{
	// 	Collection:      "PlayerData",
	// 	Key:             "Mission_Data",
	// 	UserID:          userId,
	// 	Value:           string(missionData),
	// 	PermissionRead:  2, // Only the server and owner can read
	// 	PermissionWrite: 1, // Only the server can write
	// }
	_, err = nk.StorageWrite(ctx, []*runtime.StorageWrite{dailyWrite})
	if err != nil {
		logger.Error("Error create storage user %v", err)
		return err
	}
	return nil
}
func UpdateAvatar(ctx context.Context, logger runtime.Logger, nk runtime.NakamaModule){
	avatar := []string{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"}
	rand.Seed(time.Now().UTC().UnixNano())
	x := rand.Intn(14-0+1) + 0

	userId, _ := ctx.Value(runtime.RUNTIME_CTX_USER_ID).(string)
	username := ""
	metadata := make(map[string]interface{})
	displayName := ""
	timezone := ""
	location := ""
	langTag := ""
	avatarUrl := avatar[x]
	if err := nk.AccountUpdateId(ctx, userId, username, metadata, displayName, timezone, location, langTag, avatarUrl); err != nil {
		logger.WithField("err", err).Error("Account update error.")
	}
}