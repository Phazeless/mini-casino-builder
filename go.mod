module mini-casino-builder

go 1.17

require (
	github.com/google/uuid v1.3.0
	github.com/heroiclabs/nakama-common v1.26.0
	github.com/mitchellh/mapstructure v1.5.0
)

require (
	github.com/google/go-cmp v0.5.6 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
